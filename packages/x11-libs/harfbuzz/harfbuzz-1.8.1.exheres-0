# Copyright 2012 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="An OpenType text shaping engine"
HOMEPAGE="https://www.freedesktop.org/wiki/Software/${PN}"
DOWNLOADS="https://www.freedesktop.org/software/${PN}/release/${PNV}.tar.bz2"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    gobject-introspection
    graphite [[ description = [ Build with Graphite2 font system support ] ]]
    gtk-doc
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.34.0] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.15] )
    build+run:
        dev-libs/glib:2[>=2.19.1]
        dev-libs/icu:=
        media-libs/fontconfig
        media-libs/freetype:2[>=2.4.2]
        x11-libs/cairo[>=1.8.0]
        graphite? ( x11-libs/graphite2 )
    test:
        dev-lang/python:*
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
    --with-cairo
    --with-fontconfig
    --with-freetype
    --with-glib
    --with-gobject
    --with-icu
    --without-coretext
    --without-directwrite
    --without-uniscribe
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    gtk-doc
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'graphite graphite2'
)

src_prepare() {
    # TODO: Fix this properly upstream
    edo sed \
        -e "s:nm:$(exhost --tool-prefix)nm:g" \
        -e "s:c++filt:$(exhost --tool-prefix)c++filt:g" \
        -i src/check-symbols.sh

    # Temporarily skip failing test:
    # https://github.com/behdad/harfbuzz/issues/437
    edo sed -e "/tests\/fuzzed.tests/d" -i test/shaping/Makefile.am

    autotools_src_prepare
}

