# Copyright 2015-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt qmake [ slot=5 ]

export_exlib_phases src_prepare src_configure src_compile src_install

SUMMARY="Qt Cross-platform application framework: Integrate chromium into Qt"

LICENCES="LGPL-3"
MYOPTIONS="examples geolocation pulseaudio
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=2.7.5&<3]
        dev-lang/yasm
        sys-devel/ninja
        virtual/pkg-config
    build+run:
        app-arch/snappy
        app-spell/hunspell:=
        dev-libs/expat
        dev-libs/jsoncpp:=
        dev-libs/libevent:=
        dev-libs/libxml2:2.0
        dev-libs/libxslt
        dev-libs/nspr[>=4.0]
        dev-libs/nss[>=3.14.3] [[ note = [ see src/3rdparty/chromium/crypto/nss_util.cc ] ]]
        dev-libs/protobuf:=
        dev-libs/re2
        media/ffmpeg[>=3.0]
        media-libs/fontconfig
        media-libs/freetype
        media-libs/libvpx:=[>=1.6.0-r1]
        media-libs/libwebp:=[>=0.4]
        media-libs/opus
        sys-apps/dbus
        sys-apps/pciutils
        sys-sound/alsa-lib
        x11-dri/libdrm
        x11-dri/mesa
        x11-libs/libX11
        x11-libs/libXcomposite
        x11-libs/libXcursor
        x11-libs/libXdamage
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXi
        x11-libs/libXrandr
        x11-libs/libXrender
        x11-libs/libXScrnSaver
        x11-libs/libXtst
        x11-libs/libxcb
        x11-libs/qtbase:${SLOT}[>=${PV}][gui]
        x11-libs/qtdeclarative:${SLOT}[>=${PV}]
        x11-libs/qtquickcontrols:${SLOT}[>=${PV}]
        x11-libs/qttools:${SLOT}[>=${PV}]   [[ note = [ QtDesigner ] ]]
        x11-libs/qtwebchannel:${SLOT}[>=${PV}]
        x11-libs/qtxmlpatterns:${SLOT}[>=${PV}]
        examples? ( x11-libs/qtquickcontrols:${SLOT}[>=${PV}] )
        geolocation? ( x11-libs/qtlocation:${SLOT}[>=${PV}] )
        providers:eudev? ( sys-apps/eudev )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:systemd? ( sys-apps/systemd )
        pulseaudio? (
            media-sound/pulseaudio
        )
"

if ever at_least 5.10.1 ; then
    DEPENDENCIES+="
        build:
            dev-util/gperf
            sys-devel/bison
            sys-devel/flex
        build+run:
            dev-libs/glib:2[>=2.32.0]
            dev-libs/icu:=[>=53]
            media-libs/lcms2
            media-libs/libpng:=[>=1.6.0]
            x11-libs/harfbuzz[>=1.4.2]
    "
else
    DEPENDENCIES+="
        build+run:
            dev-libs/glib:2
            dev-libs/icu:=
            media-libs/libpng:=
            x11-libs/harfbuzz[>=1.2.0]
    "
fi

# 83e790dae14725347180bb79e463c5f242616b8d in qtbase.git (part of v5.9.2):
# "We currently don't support unbundling SRTP because Chromium uses a too
# new unreleased version, but we were still testing for it and claiming to
# use the system one if found."

qtwebengine_src_prepare() {
    default

    # Since qtwebengine bundles chromium we need to use the same workaround,
    # add appropriate symlinks and add them to the PATH...
    local dir=${WORKBASE}/symlinked-build-tools
    edo mkdir -p ${dir}
    edo ln -s /usr/$(exhost --target)/bin/$(exhost --target)-pkg-config ${dir}/pkg-config
    edo ln -s /usr/$(exhost --target)/bin/$(exhost --target)-ar ${dir}/ar
    edo ln -s /usr/$(exhost --target)/bin/$(exhost --target)-gcc ${dir}/gcc
    edo ln -s /usr/$(exhost --target)/bin/$(exhost --target)-g++ ${dir}/g++
    edo ln -s /usr/$(exhost --target)/bin/$(exhost --target)-nm ${dir}/nm
    edo ln -s /usr/$(exhost --target)/bin/$(exhost --target)-readelf ${dir}/readelf
    export PATH="${dir}:${PATH}"

    # Needed for gcc >= 6 detection
    edo sed -e "/gcc:!clang/s:gcc:$(exhost --tool-prefix)gcc:" \
            -i src/core/config/linux.pri

    if ever at_least 5.10.0 ; then
        edo sed -e "/!gcc:!clang/s:gcc:$(exhost --tool-prefix)gcc:" \
                -i mkspecs/features/platform.prf
    else
        edo sed -e "/!gcc:!clang/s:gcc:$(exhost --tool-prefix)gcc:" \
                -i mkspecs/features/functions.prf
    fi
    edo sed -e "/check_call/ s/strip/$(exhost --build)-strip/" \
            -i src/3rdparty/chromium/tools/gn/bootstrap/bootstrap.py
}

qtwebengine_src_configure() {
    export NINJA_PATH=/usr/$(exhost --target)/bin/ninja

    if option geolocation ; then
        gyp_config+=( QT += positioning )
    else
        gyp_config+=( QT -= positioning )
    fi

    if option examples ; then
        EQMAKE_PARAMS+=( QT_BUILD_PARTS+=examples )
    else
        EQMAKE_PARAMS+=( QT_BUILD_PARTS-=examples )
    fi

    if ever at_least 5.10.0 ; then
        eqmake -- \
            -webengine-alsa \
            -no-webengine-embedded-build \
            -system-webengine-icu \
            -system-webengine-ffmpeg \
            -system-webengine-opus \
            -system-webengine-webp \
            -webengine-pepper-plugins \
            -webengine-printing-and-pdf \
            -webengine-proprietary-codecs \
            -webengine-spellchecker \
            -webengine-v8-snapshot \
            -webengine-webrtc \
            $(qt_enable pulseaudio webengine-pulseaudio)
    else
        eqmake -- \
            -no-embedded \
            -system-ffmpeg \
            -system-opus \
            -system-webp \
            -pepper-plugins \
            -printing-and-pdf \
            -proprietary-codecs \
            -spellchecker \
            -webengine-icu \
            $(qt_enable pulseaudio)
    fi
}

qtwebengine_src_compile() {
    export NINJAFLAGS="-v -j${EXJOBS:-1}"

    # Seems qtwebengine gets confused by our AR env variable, perhaps we can set
    # ar_cmd in src/3rdparty/chromium/third_party/libvpx/unpack_lib_posix.gypi instead?
    unset AR
    default
}

qtwebengine_src_install() {
    default

    # remove references to build dir
    edo sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" "${IMAGE}"/usr/$(exhost --target)/lib/libQt5*.prl
}

