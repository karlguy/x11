# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt qmake [ slot=5 ]

export_exlib_phases src_configure src_compile src_install

SUMMARY="Qt Cross-platform application framework: QtSensors"
DESCRIPTION="
The Qt Sensors API provides access to sensor hardware via QML and C++
interfaces. The Qt Sensors API also provides a motion gesture
recognition API for devices."

LICENCES="FDL-1.3 GPL-2 GPL-3 LGPL-3"
MYOPTIONS="doc examples"

DEPENDENCIES="
    build:
        doc? (
            x11-libs/qttools:${SLOT}
        )
    build+run:
        x11-libs/qtbase:${SLOT}[>=${PV}]
        x11-libs/qtdeclarative:${SLOT}[>=${PV}]
"
# NOTE: Automagic dep on the unpackaged sensorfw:
# https://git.merproject.org/mer-core/sensorfw

qtsensors_src_configure() {
    if option examples ; then
        EQMAKE_PARAMS+=( QT_BUILD_PARTS+=examples )
    else
        EQMAKE_PARAMS+=( QT_BUILD_PARTS-=examples )
    fi

    qmake_src_configure
}

qtsensors_src_compile() {
    default

    option doc && emake docs
}

qtsensors_src_install() {
    default

    if option doc ; then
        dodoc doc/${PN}.qch
        docinto html
        dodoc -r doc/${PN}
    fi

    # remove references to build dir
    edo sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" "${IMAGE}"/usr/$(exhost --target)/lib/libQt5*.prl
}

