# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt qmake [ slot=5 ]

export_exlib_phases src_configure src_compile src_install

SUMMARY="Qt Cross-platform application framework: QtNetworkAuthorization"
DESCRIPTION="
Qt Network Authorization provides a set of APIs that enable Qt applications to
obtain limited access to online accounts and HTTP services without exposing
users' passwords.
Currently, the supported authorization protocol is OAuth, versions 1 and 2.
"

LICENCES="FDL-1.3 GPL-3"
MYOPTIONS="doc examples"

DEPENDENCIES="
    build:
        doc? (
            x11-libs/qttools:${SLOT}
        )
    build+run:
        x11-libs/qtbase:${SLOT}[>=${PV}][gui]
"

qtnetworkauth_src_configure() {
    if option examples ; then
        EQMAKE_PARAMS+=( QT_BUILD_PARTS+=examples )
    else
        EQMAKE_PARAMS+=( QT_BUILD_PARTS-=examples )
    fi

    qmake_src_configure
}

qtnetworkauth_src_compile() {
    default

    option doc && emake docs
}

qtnetworkauth_src_install() {
    default

    if option doc ; then
        dodoc doc/${PN}.qch
        docinto html
        dodoc -r doc/${PN}
    fi

    # remove references to build dir
    edo sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" "${IMAGE}"/usr/$(exhost --target)/lib/libQt5*.prl
}

