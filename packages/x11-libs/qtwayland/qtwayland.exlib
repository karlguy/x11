# Copyright 2014 Jorge Aparicio
# Copyright 2014-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt qmake [ slot=5 ]

export_exlib_phases src_configure

SUMMARY="Wayland plugin for Qt"
HOMEPAGE="http://qt-project.org/wiki/${PN}"

LICENCES+="
    GPL-3
    HPND [[ note = [ wayland-protocol, wayland-txt-input-unstable ] ]]
    MIT  [[ note = [ wayland-{ivi-extension-protocol,xdg-shell-protocol} ] ]]
"
MYOPTIONS="
    examples
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/glib:2
        sys-libs/wayland[>=1.6.0]
        x11-dri/mesa
        x11-libs/libX11
        x11-libs/libXcomposite
        x11-libs/libxkbcommon[>=0.2.0]
        x11-libs/qtbase:${SLOT}[>=${PV}][gui]
        x11-libs/qtdeclarative:${SLOT}[>=${PV}]
"

qtwayland_src_configure() {
    if option examples ; then
        EQMAKE_PARAMS+=( QT_BUILD_PARTS+=examples )
    else
        EQMAKE_PARAMS+=( QT_BUILD_PARTS-=examples )
    fi

    qmake_src_configure
}

