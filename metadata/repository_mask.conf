(
    dev-libs/libepoxy[~scm]
    x11-apps/xinit[~scm]
    x11-apps/xkeyboard-config[~scm]
    x11-dri/glu[~scm]
    x11-dri/libdrm[~scm]
    x11-dri/mesa[~scm]
    x11-dri/mesa-demos[~scm]
    x11-drivers/glamor[~scm]
    x11-drivers/xf86-input-evdev[~scm]
    x11-drivers/xf86-input-keyboard[~scm]
    x11-drivers/xf86-input-libinput[~scm]
    x11-drivers/xf86-input-mouse[~scm]
    x11-drivers/xf86-input-synaptics[~scm]
    x11-drivers/xf86-input-wacom[~scm]
    x11-drivers/xf86-video-amdgpu[~scm]
    x11-drivers/xf86-video-ati[~scm]
    x11-drivers/xf86-video-dummy[~scm]
    x11-drivers/xf86-video-intel[~scm]
    x11-drivers/xf86-video-nouveau[~scm]
    x11-drivers/xf86-video-virgl[~scm]
    x11-drivers/xf86-video-wayland[~scm]
    x11-misc/rofi[~scm]
    x11-libs/libevdev[~scm]
    x11-libs/libpciaccess[~scm]
    x11-libs/libX11[~scm]
    x11-libs/libXext[~scm]
    x11-libs/libXi[~scm]
    x11-libs/libxkbcommon[~scm]
    x11-libs/pixman[~scm]
    x11-libs/qtbase:5[~scm]
    x11-libs/qtdeclarative:5[~scm]
    x11-libs/qtdoc:5[~scm]
    x11-libs/qtgraphicaleffects:5[~scm]
    x11-libs/qtimageformats:5[~scm]
    x11-libs/qtmultimedia:5[~scm]
    x11-libs/qtquickcontrols:5[~scm]
    x11-libs/qtquickcontrols2:5[~scm]
    x11-libs/qtscript:5[~scm]
    x11-libs/qtserialport:5[~scm]
    x11-libs/qtsvg:5[~scm]
    x11-libs/qttools:5[~scm]
    x11-libs/qttranslations:5[~scm]
    x11-libs/qtvirtualkeyboard:5[~scm]
    x11-libs/qtwayland:5[~scm]
    x11-libs/qtwebchannel:5[~scm]
    x11-libs/qtwebengine:5[~scm]
    x11-libs/qtx11extras:5[~scm]
    x11-libs/qtxmlpatterns:5[~scm]
    x11-libs/virglrenderer[~scm]
    x11-proto/dri2proto[~scm]
    x11-proto/fontsproto[~scm]
    x11-proto/inputproto[~scm]
    x11-proto/randrproto[~scm]
    x11-proto/xextproto[~scm]
    x11-proto/xproto[~scm]
    x11-server/xorg-server[~scm]
    x11-themes/oxygen-gtk2[~scm]
    x11-themes/oxygen-gtk3[~scm]
    x11-utils/util-macros[~scm]
    x11-utils/xcb-util[~scm]
    x11-wm/fluxbox[~scm]
) [[
    *author = [ Exherbo developers ]
    *token = scm
    *description = [ Mask scm versions ]
]]

media-libs/freetype:2[<2.9-r1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 02 Mar 2018 ]
    token = security
    description = [ CVE-2018-6942 ]
]]

x11-apps/xkeyboard-config[<=2.4.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 20 Jan 2012 ]
    token = security
    description = [ CVE-2012-0064 ]
]]

app-text/poppler[<0.56.0] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 22 Jun 2017 ]
    token = security
    description = [ CVE-2017-9775 ]
]]

x11-server/xorg-server[<1.19.5] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 13 Oct 2017 ]
    token = security
    description = [ CVE-2017-121{76,77,78,79,80,81,82,83,84,85,86,87} ]
]]

x11-libs/libXv[<1.0.8] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-1989 and CVE-2013-2066 ]
]]

x11-libs/libXrandr[<1.4.1] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-1986 ]
]]

x11-libs/libXext[<1.3.2] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-1982 ]
]]

x11-libs/libXtst[<1.2.2] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-2063 ]
]]

x11-libs/libXxf86vm[<1.1.3] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-2001 ]
]]

x11-libs/libXt[<1.1.4] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-2002 and CVE-2013-2005 ]
]]

x11-libs/libXres[<1.0.7] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-1988 ]
]]

x11-libs/libXinerama[<1.1.3] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-1985 ]
]]

x11-libs/libXcursor[<1.1.15] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 29 Nov 2017 ]
    token = security
    description = [ CVE-2017-16612 ]
]]

x11-libs/libXfixes[<5.0.1] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 02 Jun 2013 ]
    token = security
    description = [ CVE-2013-1983 ]
]]

x11-libs/libXp[<1.0.2] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Jun 2013 ]
    token = security
    description = [ CVE-2013-2062 ]
]]

x11-libs/libXxf86dga[<1.1.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Jun 2013 ]
    token = security
    description = [ CVE-2013-1991 and CVE-2013-2000 ]
]]

x11-libs/libdmx[<1.1.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Jun 2013 ]
    token = security
    description = [ CVE-2013-1992 ]
]]

x11-libs/libFS[<1.0.5] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 03 Jun 2013 ]
    token = security
    description = [ CVE-2013-1996 ]
]]

x11-libs/libX11[<1.6] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 06 Jun 2013 ]
    token = security
    description = [ CVE-2013-1981, CVE-2013-1997, CVE-2013-2004 ]
]]

x11-libs/libxcb[<1.9.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 09 Jun 2013 ]
    token = security
    description = [ CVE-2013-2064 ]
]]

x11-libs/libXi[<1.7.2] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 05 Jul 2013 ]
    token = security
    description = [ CVE-2013-1998, CVE-2013-1984, CVE-2013-1995 ]
]]

x11-libs/libXrender[<0.9.8] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 05 Jul 2013 ]
    token = security
    description = [ CVE-2013-1987 ]
]]

x11-libs/libXvMC[<1.0.8] [[
    author = [ Paul Seidler <sepek@exherbo.org> ]
    date = [ 05 Jul 2013 ]
    token = security
    description = [ CVE-2013-1999 ]
]]

x11-libs/libXfont[<1.5.4] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 29 Nov 2017 ]
    token = security
    description = [ CVE-2017-16611 ]
]]

net-libs/libvncserver[<0.9.11] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 Jan 2017 ]
    token = security
    description = [ CVE-2016-994{1,2}  ]
]]

x11-libs/libvdpau[<1.1.1] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 01 Sep 2015 ]
    token = security
    description = [ CVE-2015-5{198,199,200} ]
]]

(
    x11-libs/cairo[>=1.15&<1.16]
) [[
    *author = [ Marc-Antoine Perennou <keruspe@exherbo.org> ]
    *date = [ 15 Jan 2016 ]
    *token = testing
    *description = [ Development release ]
]]

x11-libs/qtwebengine:5[<5.9.5] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 23 Jan 2018 ]
    token = security
    description = [ CVE-2017-15429,
                    CVE-2018-{6031,6033,6034,6037,6038,6040,6047,6048,6051,
                        6052,6054,6060,6062,6064,6069,6071,6073,6076,6079,
                        6081,6082}
                    Security Bugs 770734,774833,789764,798410,806122 ]
]]

media-libs/fontconfig[<2.12.1] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 15 Aug 2016 ]
    token = security
    description = [ CVE-2016-5384 ]
]]

x11-libs/gtk+:4.0 [[
    author = [ Marc-Antoine Perennou <keruspe@exherbo.org> ]
    date = [ 23 Nov 2016 ]
    token = testing
    description = [ Development release ]
]]

x11-libs/libXpm[<3.5.12] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 23 May 2017 ]
    token = security
    description = [ CVE-2016-10164 ]
]]

app-text/poppler[>=0.58.0] [[
    author = [ Heiko Becker <heirecka@exherbo.org> ]
    date = [ 06 Sep 2017 ]
    token = testing
    description = [ Breaks at least texlive, luatex and inkscape ]
]]

x11-libs/libXfont2[<2.0.3] [[
    author = [ Timo Gurr <tgurr@exherbo.org> ]
    date = [ 29 Nov 2017 ]
    token = security
    description = [ CVE-2017-16611 ]
]]

(
    x11-proto/bigreqsproto
    x11-proto/compositeproto
    x11-proto/damageproto
    x11-proto/dmxproto
    x11-proto/dri2proto
    x11-proto/dri3proto
    x11-proto/evieext
    x11-proto/fixesproto
    x11-proto/fontcacheproto
    x11-proto/fontsproto
    x11-proto/glproto
    x11-proto/inputproto
    x11-proto/kbproto
    x11-proto/presentproto
    x11-proto/printproto
    x11-proto/randrproto
    x11-proto/recordproto
    x11-proto/renderproto
    x11-proto/resourceproto
    x11-proto/scrnsaverproto
    x11-proto/trapproto
    x11-proto/videoproto
    x11-proto/xcmiscproto
    x11-proto/xextproto
    x11-proto/xf86bigfontproto
    x11-proto/xf86dgaproto
    x11-proto/xf86driproto
    x11-proto/xf86miscproto
    x11-proto/xf86rushproto
    x11-proto/xf86vidmodeproto
    x11-proto/xineramaproto
    x11-proto/xproto
) [[
    *author = [ Rasmus Thomsen <cogitri@exherbo.org> ]
    *token = pending-removal
    *description = [ Mask soon to be removed *protos which have been replaced by xorgproto ]
]]
